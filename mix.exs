defmodule FcdApi.MixProject do
  use Mix.Project

  def project do
    [
      app: :food_compositon_database_api,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      {:httpoison, "~> 1.0", override: true},
      {:poison, "~> 3.1"},
      {:exvcr, "~> 0.10.0", only: [:test, :dev], runtime: false},
      {:mix_test_watch, "~> 0.5", only: [:dev], runtime: false},
      {:dialyxir, "~> 0.5.1", only: [:test, :dev], runtime: false},
      {:credo, "~> 0.8.10", only: [:test, :dev], runtime: false}
    ]
  end
end
