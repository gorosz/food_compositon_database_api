defmodule FCDApiTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney
  # Example searcg API url:  https://api.nal.usda.gov/ndb/search/?format=json&q=butter&sort=n&max=25&offset=0&api_key=DEMO_KEY
  # Example report API url:  https://api.nal.usda.gov/ndb/reports/V2?ndbno=01009&ndbno=01009&ndbno=45202763&type=b&format=json&api_key=DEMO_KEY

  setup_all do
    ExVCR.Config.cassette_library_dir("fixture/vcr_cassettes", "fixture/custom_cassettes")
    HTTPoison.start
  end

  test "can fetch one nutrition data by id" do
    use_cassette "found_one_food_nutritions", custom: true do
      nutrients_ids = ["01009"]
      nutrients = query_nutrients(nutrients_ids)
      assert nutrients == [%Nutrients{id: "01009", name: "food1", ch: 1}]
    end
  end

  test "can fetch more nutrition data by id at once" do
    use_cassette "found_more_food_nutritions", custom: true do
      nutrients_ids = ["01009", "01010"]
      nutrients = query_nutrients(nutrients_ids)
      assert nutrients == [%Nutrients{id: "01009", name: "food1", ch: 1}, %Nutrients{id: "01010", name: "food2", ch: 2}]
    end
  end

  test "not found food creates empty nutritions" do
    use_cassette "not_found_food_nutritions" do
      nutrients_ids = ["not_existing"]
      nutrients = query_nutrients(nutrients_ids)
      assert nutrients == [%Nutrients{id: "not_existing", name: "N/A", ch: 0}]
    end
  end

  test "not found foods creates empty nutritions" do
    use_cassette "not_found_more_food_nutritions" do
      nutrients_ids = ["not_existing1", "not_existing2"]
      nutrients = query_nutrients(nutrients_ids)
      assert nutrients == [%Nutrients{id: "not_existing1", name: "N/A", ch: 0}, %Nutrients{id: "not_existing2", name: "N/A", ch: 0}]
    end
  end

  defp query_nutrients(ids) do
    FcdApi.get_nutrient_by_food_ids(ids)
  end
end
