defmodule Nutrients do
  @moduledoc ""
  defstruct [:id, :name, :ch]

  def from_ndb_json(%{"food" => ndb_food}) do
    %Nutrients{
      name: ndb_food["desc"]["name"],
      ch: get_ch_values(ndb_food["nutrients"]),
      id: ndb_food["desc"]["ndbno"]
    }
  end

  def from_ndb_json(%{"error" => error}) do
    %Nutrients{name: "N/A", ch: 0, id: parse_id_from_error(error)}
  end

  defp get_ch_values(nutrients) do
    charbohydrates = Enum.filter(nutrients, fn n -> n["nutrient_id"] == "205" end)
    [ch | _] = charbohydrates
    {v, ""} = Integer.parse(ch["value"])
    v
  end

  defp parse_id_from_error(error) do
    [_, t] = Regex.run(~r/(?s)ndbno (.*\S)/, error)
    t
  end
end
