defmodule FcdApi do
  @moduledoc """
  Documentation for FcdApi.
  """
  def get_nutrient_by_food_ids(food_ids) do
    food_ids
    |> prepare_url_parameters
    |> assemble_url
    |> query_nutrients
    |> convert_to_json
    |> convert_to_nutrients
  end
@spec prepare_url_parameters([String.t]) :: String.t
  defp prepare_url_parameters(ids) do
    base_parameters = "type=b&format=json&api_key=DEMO_KEY"
    ids = "ndbno=#{Enum.join(ids, "&ndbno=")}"
    "#{ids}&#{base_parameters}"
  end

  defp assemble_url(parameters) do
    base_url = "https://api.nal.usda.gov/ndb/V2/reports"
    "#{base_url}?#{parameters}"
  end

  defp query_nutrients(url) do
    HTTPoison.get(url)
  end

  defp convert_to_json({:ok, response}) do
    Poison.decode(response.body)
  end

  defp convert_to_nutrients({:ok, response_json}) do
    Enum.map(response_json["foods"], &Nutrients.from_ndb_json/1)
  end
end
