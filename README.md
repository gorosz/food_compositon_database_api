# FcdApi

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `food_compositon_database_api` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:food_compositon_database_api, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/food_compositon_database_api](https://hexdocs.pm/food_compositon_database_api).

